var __dirname = "./public"
var express = require("express");
var app     = express();
app.use(express.static(__dirname));

app.get('/',function(req,res){
    res.sendFile('index.html');
});

app.listen(80);

console.log("Running at Port 80");